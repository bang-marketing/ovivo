<?php
/*
Plugin Name: BANG - Lockdown
*/


// LOCKDOWN ALL BUT ADMINS
function blockusers_init() {
    if ( is_admin() && ! current_user_can( 'administrator' ) &&
       ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
        $logout_url = wp_login_url().'?mode=lockdown';
 	    // wp_logout();
	    wp_redirect( $logout_url, 302 );
    }
}

add_action( 'init', 'blockusers_init' );


// LOGIN NOTICE
function my_login_message() {

        $message = '<p class="message error" style="border-left-color: #dc3232;"><b style="font-size: 16px;display: block;margin-bottom: 5px;">LE SITE EST EN MODE "LOCKDOWN"</b>Seul les super-administrateurs peuvent accéder à l\'admin.</p><br>';

        if( $_GET['mode'] == 'lockdown' ){
            $message .= '<p class="error message">Votre compte ne peut accéder l\'admin pour le moment.</p>';
        }
    	return $message;
}
add_filter('login_message', 'my_login_message');


// ADMIN AREA NOTICE
function my_admin_notice() {
    ?>
    <div class="notice notice-warning">
        <p><?php _e( 'Le site est en mode <strong>BANG-LOCKDOWN!</strong>', 'bang-lockdown' ); ?></p>
    </div>
    <?php
}
add_action( 'admin_notices', 'my_admin_notice' );


?>
