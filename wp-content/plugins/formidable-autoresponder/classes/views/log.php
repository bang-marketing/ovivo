<?php
/**
 * Display the log for a form.
 *
 * @package formidable-autoresponder
 */

?>
<pre>
	<?php
	/* translators: %s: Timestamp */
	echo esc_html( sprintf( __( 'Note: Times are in UTC +0, which is now %s', 'formidable-autoresponder' ), date( 'Y-m-d H:i:s' ) ) );
	?>
</pre>
<pre><?php echo wp_kses_post( $log ); ?></pre>
