<?php
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 */
define('AUTH_KEY',         'N0pBbutHFuLEKwdmRrcU0mrsV66Lp6efbwsD7yf6QhNzNT1RjCMdHGSTrePMVqhu');
define('SECURE_AUTH_KEY',  '3xvMvIaIKt4MA3zneLxPFPzu74JJAb8mE8Pa1RXdhWCHVHtS5bAcrVuGjQ0xjncr');
define('LOGGED_IN_KEY',    'CXsszDhiBeJerHmfENRX2iqYW7EmjMbAzeChTofQxLdTc2hN5Hq5c5b3IUneg5Tj');
define('NONCE_KEY',        '49U2PCeycvnC1I0v6ez4WKgMviwiwee69K0IspotgQPVLK8YbrrESa8pCfdYD4zD');
define('AUTH_SALT',        'W4SAyx5XAPBBGoQ0uEGaFirn5bQtKDhMrm4prtzVuyVN7M3MTsrEKWrJAGdGpxjz');
define('SECURE_AUTH_SALT', 'X8vPiubt7zFQ03JacSNoBpEIx92cT3PhI70XDtDv5cFPHES0NLVcigNHfLqTvUxp');
define('LOGGED_IN_SALT',   'A3E26F28rGEJWL1PRICa5btsETWYKotANmGvMDK5WyA8TXj3W3u3ygUC4d35o3H4');
define('NONCE_SALT',       '57wVCHmfsVjuuqhHNb6rGUDtrKKSW5KRmRpetCPfjxbjuncraMfoDGN6NrUc0GRh');
